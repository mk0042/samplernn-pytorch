FROM nvidia/cuda:9.0-cudnn7-devel-ubuntu16.04
#FROM nvidia/cuda:8.0-cudnn5-devel

#RUN apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y # Good practice, update all the packages.

RUN apt-get update && apt-get install -y \
  git-all \
  sudo \
  expat \
  openssh-server \
  sssd
  
RUN mkdir /var/run/sshd
